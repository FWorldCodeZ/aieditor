# 菜单配置


```javascript
toolbarKeys:["undo", "redo", "brush", "eraser", "|"
    , "title", "font-family", "font-size", "|"
    ,  "bold", "italic", "underline"
    , "strike", "link", "code", "subscript", "superscript"
    , "hr", "todo", "emoji", "divider", "highlight", "font-color", "|"
    , "align", "line-height", "|"
    , "bullet-list", "ordered-list", "indent-decrease", "indent-increase", "break", "|"
    , "image", "video", "attachment", "quote", "code-block", "table", "|"
    ,  "printer", "fullscreen", "ai"
]
```
